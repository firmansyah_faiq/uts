class Game:
    def __init__(self, nama,jenis,type,kategori):
        self.nama = nama
        self.jenis = jenis
        self.type = type
        self.kategori = kategori

    def ketentuanUsia(self):
        if self.jenis == "online":
            if self.kategori == "dewasa":
                print( "{} game {} ini merupakan game dengan type {} hanya cocok di mainkan oleh orang yang berusia 20 tahun ke atas".format(self.nama,self.jenis,self.type,))
            elif self.kategori == "remaja":
                print( "{} game {} ini merupakan game dengan type {} hanya cocok di mainkan oleh orang yang berusia 10 tahun ke atas".format(self.nama,self.jenis,self.type,))
            else:
                print( "{} game {} ini merupakan game dengan type {} hanya cocok di mainkan oleh orang yang berusia 4 tahun ke atas".format(self.nama,self.jenis,self.type,))
        elif self.jenis == "ofline":
            if self.kategori == "dewasa":
                print( "{} game {} ini merupakan game dengan type {} hanya cocok di mainkan oleh orang yang berusia 15 tahun ke atas".format(self.nama,self.jenis,self.type,))
            elif self.kategori == "remaja":
                print( "{} game {} ini merupakan game dengan type {} hanya cocok di mainkan oleh orang yang berusia 7 tahun ke atas".format(self.nama,self.jenis,self.type,))
            else:
                print( "{} game {} ini merupakan game dengan type {} hanya cocok di mainkan oleh orang yang berusia 2 tahun ke atas".format(self.nama,self.jenis,self.type,))

nama = input("nama game : ")
jenis = input("jenis game (online/ofline) : ")
type = input("Type game (contoh : Moba) : ")
kategori = input("kategori game [anak-anak/remaja/dewasa] : ")

game = Game(nama,jenis,type,kategori)

game.ketentuanUsia()